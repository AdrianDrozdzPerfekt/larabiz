<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //Form::component(NAZWA, PLIK_WIDOKU, PARAMETRY)
        Form::component('bsText', 'components.form.text', ['name', 'value' => null, 'label' => null, 'attributes' => []]);
        Form::component('bsTextarea', 'components.form.textarea', ['name', 'value' => null, 'label' => null, 'attributes' => []]);
        Form::component('bsSubmit', 'components.form.submit', ['name', 'attributes' => []]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
