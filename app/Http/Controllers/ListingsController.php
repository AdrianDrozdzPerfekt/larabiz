<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Listing;

class ListingsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $listings = Listing::orderBy('created_at', 'desc')->get();
        return view('listings')->with('listings', $listings);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('createlisting');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //walidacja i ewentualny powrót z błędami walidacji
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone' => 'required',
            'bio' => 'required',
            'website' => 'required',
        ]);

        //utowrzenie obiektu klasy Listing
        $listing = new Listing();
        $listing->name = $request->input('name');
        $listing->email = $request->input('email');
        $listing->phone = $request->input('phone');
        $listing->address = $request->input('address');
        $listing->website = $request->input('website');
        $listing->bio = $request->input('bio');
        $listing->user_id = auth()->user()->id;

        //proba zapisania danych i przekierowanie na 
        //index listingow lub create w zaleznosci od sukcesu
        try {
            $listing->save();
            return redirect('/dashboard')
                    ->with('success', 'Dodano wizytówkę');
        } catch (\Exception $e) {
            return redirect('/dashboard')
                    ->with('error', 'Nie dodano wizytówki! ' . $e->getMessage());
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $listing = Listing::findOrFail($id);

        if ($listing->user_id != auth()->user()->id) {
            return redirect('/')->with('error', 'Brak dostępu');
        }

        return view('edit')->with('listing', $listing);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //walidacja i ewentualny powrót z błędami walidacji
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'address' => 'required',
            'phone' => 'required',
            'bio' => 'required',
            'website' => 'required',
        ]);

        //pobranie obiektu klasy Listing
        $listing = Listing::findOrFail($id);
        if ($listing->user_id != auth()->user()->id) {
            return redirect('/')->with('error', 'Brak dostępu');
        }
        $listing->name = $request->input('name');
        $listing->email = $request->input('email');
        $listing->phone = $request->input('phone');
        $listing->address = $request->input('address');
        $listing->website = $request->input('website');
        $listing->bio = $request->input('bio');
        $listing->user_id = auth()->user()->id;

        //proba zapisania danych i przekierowanie na 
        //index listingow lub create w zaleznosci od sukcesu
        try {
            $listing->save();
            return redirect('/dashboard')
                    ->with('success', 'Zapisano wizytówkę');
        } catch (\Exception $e) {
            return redirect('/dashboard')
                    ->with('error', 'Nie zapisano wizytówki! ' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $listing = Listing::findOrFail($id);
        if ($listing->user_id != auth()->user()->id) {
            return redirect('/')->with('error', 'Brak dostępu');
        }
        if ($listing->delete()) {
            return redirect('/dashboard')
                    ->with('success', 'Usunięto wizytówkę');
        } else {
            return redirect('/dashboard')
                    ->with('error', 'Nie usunięto wizytówki');
        }
    }
}
