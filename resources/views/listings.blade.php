@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard
                    <span class="pull-right">
                        <a href="{{ route('listings.create') }}" class="btn btn-success btn-xs">Add listing</a>
                    </span>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (count($listings))
                       <ul class="list-group">
                           @foreach($listings as $listing)
                            <li class="list-group-item">
                                <a href="/listings/{{$listing->id}}/edit">{{$listing->name}}</a>
                            </li>
                           @endforeach
                       </ul>
                    @else
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Brak firm!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
