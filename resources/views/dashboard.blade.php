@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Dashboard
                    <span class="pull-right">
                        <a href="{{ route('listings.create') }}" class="btn btn-success btn-xs">Add listing</a>
                    </span>
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h3>Twoje firmy</h3>
                    @if (count($listings))
                       <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Nazwa firmy</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($listings as $listing)
                                    <tr>
                                        <td>{{$listing->name}}</td>
                                        <td>
                                            <a href="{{ route('listings.edit', ['id' => $listing->id]) }}" class="pull-right btn btn-primary">Edycja</a>
                                        </td>
                                        <td>
                                            {!! Form::open([
                                                'action' => ['ListingsController@destroy', 'id' => $listing->id ], 
                                                'method' => 'POST', 
                                                'class' => 'pull-right',
                                                'onsubmit' => 'return confirm("Czy na pewno usunąć")'
                                                ]) 
                                            !!}
                                                {{ Form::hidden('_method', 'DELETE')}}
                                                {{ Form::bsSubmit('Usuń', ['class' => 'btn btn-danger'])}}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table> 
                    @else
                        <div class="alert alert-warning">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <strong>Uwaga!</strong> Brak firm!
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
