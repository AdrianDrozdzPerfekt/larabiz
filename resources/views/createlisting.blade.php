@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Create Listing</h3>
			</div>
			<div class="panel-body">
				{!! Form::open(['action' => 'ListingsController@store', 'method' => 'POST'])!!}
					{{ Form:: bsText('name', null, 'Nazwa firmy',  ['placeholder' => 'Nazwa firmy'])}}
					{{ Form:: bsText('email', null, 'Email firmy',  ['placeholder' => 'Email firmy'])}}
					{{ Form:: bsText('website', null, 'WWW firmy',  ['placeholder' => 'WWW firmy'])}}
					{{ Form:: bsText('phone', null, 'Tel firmy',  ['placeholder' => 'Tel firmy'])}}
					{{ Form:: bsText('address', null, 'Adres firmy',  ['placeholder' => 'Adres firmy'])}}
					{{ Form:: bsTextarea('bio', null, 'Opis firmy',  ['placeholder' => 'Opis firmy'])}}
					{{ Form:: bsSubmit('Dodaj', ['class' => 'btn btn-success'])}}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection