@extends('layouts.app')

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h3 class="panel-title">Edycja Wizytówki</h3>
			</div>
			<div class="panel-body">
				{!! Form::open(['action' => ['ListingsController@update', 'id' => $listing->id], 'method' => 'POST'])!!}
					{{ Form::hidden('_method', 'PUT')}}
					{{ Form::bsText('name', $listing->name, 'Nazwa firmy',  ['placeholder' => 'Nazwa firmy'])}}
					{{ Form::bsText('email', $listing->email, 'Email firmy',  ['placeholder' => 'Email firmy'])}}
					{{ Form::bsText('website', $listing->website, 'WWW firmy',  ['placeholder' => 'WWW firmy'])}}
					{{ Form::bsText('phone', $listing->phone, 'Tel firmy',  ['placeholder' => 'Tel firmy'])}}
					{{ Form::bsText('address', $listing->address, 'Adres firmy',  ['placeholder' => 'Adres firmy'])}}
					{{ Form::bsTextarea('bio', $listing->bio, 'Opis firmy',  ['placeholder' => 'Opis firmy'])}}
					{{ Form::bsSubmit('Zapisz', ['class' => 'btn btn-success'])}}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@endsection